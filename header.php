<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Butterfly_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-300.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-300italic.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-600.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-600italic.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-700.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-700italic.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-regular.woff2" crossorigin>
	<link rel="preload" as="font" type="font/woff2"  href="<?php echo get_stylesheet_directory_uri()?>/fonts/open-sans-v18-latin-italic.woff2" crossorigin>	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'butterfly-theme' ); ?></a>

	<header id="masthead" class="site-header">
		<div id="header-inner-wrapper">
			<div class="site-branding">
				<?php
				the_custom_logo();
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$butterfly_theme_description = get_bloginfo( 'description', 'display' );
				if ( $butterfly_theme_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $butterfly_theme_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<a class="menu-toggle" id="openMobileMenu">
					<div class="toggle-line"></div>
					<div class="toggle-line"></div>
					<div class="toggle-line"></div>				
				</a>
			<?php
			
				wp_nav_menu(
					array(
						'theme_location' => 'top_menu',
						'menu_id'        => 'top_menu',
					)
				);
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->
