<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Butterfly_Theme
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<?php dynamic_sidebar( 'footerwidgets' ); ?>
		</div><!-- .site-info -->
		<!-- footer menu -->
		<div class="footer-menu">
	<?php
			wp_nav_menu(
				array(
					'theme_location' => 'footer_menu',
				)
			)
			?>
		</div>
	</footer><!-- #colophon -->
	<div id="mobileMenu" class="modal">
	<span class="close">&times;</span>
		<div class="modal-content">

			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'mobile_menu',
						'menu_id'        => 'mobile_menu',
					)
				)
				?>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">

<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
</body>
</html>
