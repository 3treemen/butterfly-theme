<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Butterfly_Theme
 */

get_header();
?>
<div id="banner">
		<?php butterfly_banner(); ?>
</div>
<div id="content">
	<main id="primary" class="site-main">
		<div id="home-widgets">
			<?php dynamic_sidebar( 'homewidgets' ); ?>
		</div>		
		<div id="testimonials">
			<?php home_testimonials(); ?>
		</div>
		<div id="blog">
			<?php bloglist(3); ?>
		</div>
		<div id="homebottom">
			<?php dynamic_sidebar( 'homebottomwidgets' ); ?>
		</div>
	</main><!-- #main -->
</div>
<?php
get_footer();
