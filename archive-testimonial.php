<?php
/**
 * The template for displaying archive pages
 *
 * 
 * Template Name: Testimonial listing

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Butterfly_Theme
 * 
 */

get_header();
?>
<div id="content" class="testimonials">

	<main id="primary" class="site-main">
        <?php
    the_title( '<h1 class="entry-title">', '</h1>' ); 
    echo '<div class="intro">';
    the_content();
    echo '</div>';
            $args = array ( 
            'post_type' => 'testimonial',
            'orderby' => 'menu_order',
            'order' => 'ASC',
          );
          $query = new WP_Query( $args );

        ?>
        <div class="grid">
<?php
 if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
	  $query->the_post();
	  $functie = rwmb_meta('testimonial-functie');
      $naam = rwmb_meta('testimonial-naam' );
      $quote = rwmb_meta('testimonial-quote');
      $title = get_the_title();
      $featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'tiny');
      echo "<div class='grid-item'>";
        echo the_title('<h3>', '</h3>');
        echo '<div class="quote">' . $quote . '</div>';
        echo '<div class="persoon">';  
        echo '<img class="portret" src="'.$featured_image_url.'" alt="'.$naam.'" />';
        echo '<p>' . $naam . '</p>';
        echo '<p>' . $functie . '</p>';
        echo '</div>';
      echo '</div>';
    }
  }
?>

        </div>

	</main><!-- #main -->
</div>
<?php
// get_sidebar();
get_footer();
