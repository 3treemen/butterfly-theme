<?php
/**
 * The template for displaying archive pages
 *
 * 
 * Template Name: Example listing

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Butterfly_Theme
 * 
 */

get_header();
?>
<div id="content" class="examples">

	<main id="primary" class="site-main">
        <?php
    the_title( '<h1 class="entry-title">', '</h1>' ); 
    echo '<div class="intro">';
    the_content();
    echo '</div>';
            $args = array ( 
            'post_type' => 'examples',
            'orderby' => 'menu_order',
            'order' => 'ASC',
          );
          $query = new WP_Query( $args );

        ?>
        <div class="grid">
<?php
 if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
	    $query->the_post();
  	  $url = get_post_meta( get_the_ID(), 'meta-url', true);
      $external = get_post_meta( get_the_ID(), 'meta-external', true);
      $title = get_the_title();
      $description = get_the_excerpt();
      $featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail');
      echo "<div class='grid-item'>";
      echo "<div class='logo'><img src=" . $featured_image_url . " alt='' /></div>";  
        echo '<div class="info">';  
        echo the_title('<h3>', '</h3>');
        echo '<p>' . $description . '</p>';
        echo '</div>';
      echo '</div>';
    }
  }
?>

        </div>

	</main><!-- #main -->
</div>
<?php
// get_sidebar();
get_footer();
