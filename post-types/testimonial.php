<?php

/**
 * Registers the `testimonial` post type.
 */
function testimonial_init() {
	register_post_type( 'testimonial', array(
		'labels'                => array(
			'name'                  => __( 'Testimonials', 'butterfly-theme' ),
			'singular_name'         => __( 'Testimonial', 'butterfly-theme' ),
			'all_items'             => __( 'All Testimonials', 'butterfly-theme' ),
			'archives'              => __( 'Testimonial Archives', 'butterfly-theme' ),
			'attributes'            => __( 'Testimonial Attributes', 'butterfly-theme' ),
			'insert_into_item'      => __( 'Insert into Testimonial', 'butterfly-theme' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'butterfly-theme' ),
			'featured_image'        => _x( 'Featured Image', 'testimonial', 'butterfly-theme' ),
			'set_featured_image'    => _x( 'Set featured image', 'testimonial', 'butterfly-theme' ),
			'remove_featured_image' => _x( 'Remove featured image', 'testimonial', 'butterfly-theme' ),
			'use_featured_image'    => _x( 'Use as featured image', 'testimonial', 'butterfly-theme' ),
			'filter_items_list'     => __( 'Filter Testimonials list', 'butterfly-theme' ),
			'items_list_navigation' => __( 'Testimonials list navigation', 'butterfly-theme' ),
			'items_list'            => __( 'Testimonials list', 'butterfly-theme' ),
			'new_item'              => __( 'New Testimonial', 'butterfly-theme' ),
			'add_new'               => __( 'Add New', 'butterfly-theme' ),
			'add_new_item'          => __( 'Add New Testimonial', 'butterfly-theme' ),
			'edit_item'             => __( 'Edit Testimonial', 'butterfly-theme' ),
			'view_item'             => __( 'View Testimonial', 'butterfly-theme' ),
			'view_items'            => __( 'View Testimonials', 'butterfly-theme' ),
			'search_items'          => __( 'Search Testimonials', 'butterfly-theme' ),
			'not_found'             => __( 'No Testimonials found', 'butterfly-theme' ),
			'not_found_in_trash'    => __( 'No Testimonials found in trash', 'butterfly-theme' ),
			'parent_item_colon'     => __( 'Parent Testimonial:', 'butterfly-theme' ),
			'menu_name'             => __( 'Testimonials', 'butterfly-theme' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'thumbnail'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'testimonial',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'testimonial_init' );

/**
 * Sets the post updated messages for the `testimonial` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `testimonial` post type.
 */
function testimonial_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['testimonial'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Testimonial updated. <a target="_blank" href="%s">View Testimonial</a>', 'butterfly-theme' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'butterfly-theme' ),
		3  => __( 'Custom field deleted.', 'butterfly-theme' ),
		4  => __( 'Testimonial updated.', 'butterfly-theme' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Testimonial restored to revision from %s', 'butterfly-theme' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Testimonial published. <a href="%s">View Testimonial</a>', 'butterfly-theme' ), esc_url( $permalink ) ),
		7  => __( 'Testimonial saved.', 'butterfly-theme' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Testimonial submitted. <a target="_blank" href="%s">Preview Testimonial</a>', 'butterfly-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testimonial</a>', 'butterfly-theme' ),
		date_i18n( __( 'M j, Y @ G:i', 'butterfly-theme' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Testimonial draft updated. <a target="_blank" href="%s">Preview Testimonial</a>', 'butterfly-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'testimonial_updated_messages' );



function testimonial_register_meta_boxes( $meta_boxes ) {
    $prefix = 'testimonial-';

    $meta_boxes[] = [
        'title'      => esc_html__( 'Testimonial', 'butterfly-theme' ),
        'id'         => 'testimonial_meta',
        'post_types' => ['testimonial'],
        'context'    => 'normal',
        'priority'   => 'high',
        'fields'     => [
            [
                'type' => 'text',
                'name' => esc_html__( 'Naam', 'butterfly-theme' ),
                'id'   => $prefix . 'naam',
            ],
            [
                'type' => 'text',
                'id'   => $prefix . 'functie',
                'name' => esc_html__( 'Functie', 'butterfly-theme' ),
			],
			[
				'type' => 'textarea',
				'id'   => $prefix . 'quote',
				'name' => esc_html__( 'Quote', 'butterfly-theme' ),
			],
			[
                'type' => 'checkbox',
                'id'   => $prefix . 'homepage',
                'name' => esc_html__( 'Show on Homepage', 'butterfly-theme' ),
            ],
        ],
    ];

    return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'testimonial_register_meta_boxes' );

